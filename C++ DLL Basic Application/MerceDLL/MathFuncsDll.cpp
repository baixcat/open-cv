// MathFuncsDll.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "MathFuncsDll.h"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>

using namespace cv;
using namespace std;

int LimpiarReflejos(unsigned int piHeight, unsigned int piWidth, intptr_t ptEntrada, intptr_t ptSalida, unsigned int ptThreshold) { return MyMathFuncs::LimpiarReflejos(piHeight, piWidth, ptEntrada,ptSalida,ptThreshold); }

bool TratarLadoOpaco(vector<vector<Point>> ContornosPrimitivos, vector<Vec4i> Hierarchy, Mat Image)
{

	double dPerimetro; //el per�metro de un contorno
	RotatedRect rrRotatedBoundingBox; //the smallest rotated bounding box that contains the contour
	
	RNG rng(12345);

	dPerimetro = 0.0;
	
	for (std::vector<std::vector<cv::Point>>::iterator it = ContornosPrimitivos.begin(); it != ContornosPrimitivos.end(); ++it)
	{

		// OBTENEMOS EL PER�METRO DEL CONTORNO
		dPerimetro = arcLength((*it), true);

		// IGNORAMOS LOS CONTORNOS QUE NO SON SIGNIFICATIVOS POR SER DEMASIADO PEQUE�OS
		if (dPerimetro > 250)
		{

			// OBTENEMOS EL RECT�NGULO M�S PEQUE�O CAPAZ DE CONTENER EL CONTORNO
			rrRotatedBoundingBox = minAreaRect(Mat(*it));

			// CALCULAMOS LAS CARACTER�STICAS DEL CONTORNO
			// - PER�METRO
			// - RATIO
			// - COORDENADASX
			// - COORDENADASY

		}
	}

	/// Draw contours
	Mat drawing = Mat::zeros(Image.size(), CV_8UC3);
	for (size_t i = 0; i< ContornosPrimitivos.size(); i++)
	{
		Scalar color = Scalar(rng.uniform(0, 255), rng.uniform(0, 255), rng.uniform(0, 255));
		drawContours(drawing, ContornosPrimitivos, (int)i, color, 2, 8, Hierarchy, 0, Point());
	}

	/// Show in a window
	namedWindow("Contours", WINDOW_AUTOSIZE);
	imshow("Contours", drawing);

	return (true);

}

int  _stdcall MyMathFuncs::LimpiarReflejos(unsigned int piHeight, unsigned int piWidth, intptr_t ptEntrada, intptr_t ptSalida, unsigned int ptThreshold)
{

	Mat ImageToFilter; //original input image

	Mat ImageGray; //original image converted to grayscale

	Mat ImageBinary; //binarized image
	const int BinaryThreshold = 150; //the thresh value with respect to which the thresholding operation is made
	const int MaxBinaryValue = 0; // the value used with the Binary thresholding operation (to set the chosen pixels)

	vector<vector<Point>> Contours;//each contour stored as a vector of points
	vector<Vec4i> Hierarchy;//information about image topology of each contour

	//Load image
	//ImageToFilter = imread("MyPic.bmp");
	//if (ImageToFilter.empty())
	//{
	//	cout << "Error: Image cannot be loaded...!!" << endl;
	//	system("pause");
	//	return -1;
	//}
	ImageToFilter = Mat(piHeight, piWidth, CV_8U, ((void *)ptEntrada));


	/// Convert image to gray and blur it
	cvtColor(ImageToFilter, ImageGray, COLOR_BGR2GRAY);
	//blur(src_gray, src_gray, Size(3, 3));

	// OBTENEMOS LA PARTE OPACA MEDIANTE UN UMBRAL ALTO Y ELIMINAMOS LAS TRANSPARENCIAS
	threshold(ImageGray, ImageBinary, BinaryThreshold, MaxBinaryValue, CV_THRESH_TOZERO);

	// Detect edges using canny
	//Canny(ImageGray, canny_output, thresh, thresh * 2, 3);

	/// Find contours
	findContours(ImageBinary, Contours, Hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE, Point(0, 0));

	bool swcheck;
	swcheck = TratarLadoOpaco(Contours, Hierarchy, ImageBinary);

	waitKey(0);
	return(0);
}


