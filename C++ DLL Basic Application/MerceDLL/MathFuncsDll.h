extern "C" _declspec(dllexport) int LimpiarReflejos(unsigned int piHeight, unsigned int piWidth, intptr_t ptEntrada, intptr_t ptSalida, unsigned int ptThreshold);

// This class is exported from the MathFuncsDll.dll
class MyMathFuncs
{
	public:
		
		static  int _stdcall LimpiarReflejos(unsigned int piHeight, unsigned int piWidth, intptr_t ptEntrada, intptr_t ptSalida, unsigned int ptThreshold);
};
