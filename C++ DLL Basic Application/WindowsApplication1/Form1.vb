﻿Imports System.Drawing.Imaging

Public Class Form1

    Private Declare Function LimpiarReflejos Lib "MerceDLL.dll" (ByVal Height As UInt32, ByVal Width As UInt32, ByVal imageDataSource As IntPtr, ByVal imageDataDest As IntPtr, ByVal threshold As UInt32) As Integer

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Dim OriginalImage As Bitmap
        Dim FilteredImage As Bitmap
        Dim OriginalImageData As bitmapdata
        Dim FilteredImageData As BitmapData
        Dim ReturnCode As Integer

        OriginalImage = New Bitmap("MyPic.bmp")
        FilteredImage = OriginalImage.Clone

        OriginalImageData = OriginalImage.LockBits(New Rectangle(0, 0, OriginalImage.Width, OriginalImage.Height), ImageLockMode.ReadWrite, OriginalImage.PixelFormat)
        FilteredImageData = FilteredImage.LockBits(New Rectangle(0, 0, FilteredImage.Width, FilteredImage.Height), ImageLockMode.ReadWrite, FilteredImage.PixelFormat)

        ReturnCode = LimpiarReflejos(OriginalImage.Height, OriginalImage.Width, OriginalImageData.Scan0, FilteredImageData.Scan0, 100)

        OriginalImage.UnlockBits(OriginalImageData)
        FilteredImage.UnlockBits(FilteredImageData)

    End Sub

End Class
